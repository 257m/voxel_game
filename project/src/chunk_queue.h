#ifndef CHUNK_QUEUE_H
#define CHUNK_QUEUE_H

typedef struct Chunk_Queue_t {
	World* w; // Back reference to parent world
	Chunk** chunks;
	void (*poll_func)(World* w, Chunk* c);
	u64 head, tail;
	u64 size;
	u8 queue_mask;
} Chunk_Queue;

extern void chunk_queue_init(Chunk_Queue* cq, World* w, void (*poll_func)(World* w, Chunk* c), u64 event_size, u8 queue_mask);
extern void chunk_queue_push(Chunk_Queue* cq, Chunk* c);
extern void chunk_queue_poll(Chunk_Queue* cq);
extern void chunk_queue_flush(Chunk_Queue* cq);

#endif /* CHUNK_QUEUE_H */
