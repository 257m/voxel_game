#ifndef MODEL_H
#define MODEL_H

#include "data_types.h"
#include "world.h"
#include <GL/gl.h>

typedef struct {
	union {
		u32 amounts [3];
		struct {
			u32 vertex_amount, texcoord_amount, normal_count;
		};
	};
	f32* vertices;
	GLint vbo;
} Model;

void model_init();
Model* model_load(char* file_str);
void model_render(World* w, Model* m);

#endif /* MODEL_H */
