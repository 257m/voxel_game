#include <GL/gl.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include "state.h"
#include "model.h"
#include "errors.h"
#include "shader_utils.h"
#include "cglm/mat4.h"
#include "world.h"

static GLuint model_program;
static GLint attribute_vertex;
static GLint attribute_texcoord;
static GLint attribute_normal;
static GLint uniform_model_mvp;

void model_init()
{
	model_program = shader_create_program("src/shaders/model.v.glsl", "src/shaders/model.f.glsl");

	attribute_vertex = glGetAttribLocation(model_program, "vertex");
  	glVertexAttribPointer(attribute_vertex, 3, GL_FLOAT, GL_FALSE, 0, 0);
  	glEnableVertexAttribArray(attribute_vertex);
	attribute_texcoord = glGetAttribLocation(model_program, "texcoord");
  	glVertexAttribPointer(attribute_texcoord, 2, GL_FLOAT, GL_FALSE, 0, sizeof(f32)*3);
  	glEnableVertexAttribArray(attribute_texcoord);
	attribute_normal = glGetAttribLocation(model_program, "normal");
  	glVertexAttribPointer(attribute_normal, 3, GL_FLOAT, GL_FALSE, 0, sizeof(f32)*5);
  	glEnableVertexAttribArray(attribute_normal);
	uniform_model_mvp = glGetUniformLocation(model_program, "mvp");
	PANIC(attribute_vertex == -1 || attribute_texcoord == -1 || attribute_normal == -1 || uniform_model_mvp == -1,
	      "Could not get location of attributes/uniform: %d,%d,%d,%d!\r\n",
	      attribute_vertex, attribute_texcoord, attribute_normal, uniform_model_mvp);
}

Model* model_load(char* file_str)
{
	FILE* f = fopen(file_str, "r");
	PANIC(!f, "Failed to open model file %s\r\n", file_str);
	Model* m = malloc(sizeof(Model));
	// Who cares about the header?
	// Nobody cares about whether the file is actually the type because I can
	// validate ahead of time
	// Skip the 80 byte header
	fseek(f, 80, SEEK_SET);
	fread(m->amounts, sizeof(*m->amounts), 3, f);
	m->vertices = malloc(*m->amounts*sizeof(f32)*8);
	fread(m->vertices, sizeof(f32)*8, *m->amounts, f);
	fclose(f);
	m->vbo = glGenBuffers(1, &m->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m->vbo);
	glBufferData(GL_ARRAY_BUFFER, *m->amounts*sizeof(f32)*8, m->vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	return m;
}

void model_render(World* w, Model* m)
{
	glUseProgram(model_program);
	glBindBuffer(GL_ARRAY_BUFFER, m->vbo);
	mat4 view;
	vec3 center;
	glm_vec3_add(w->position, w->lookat, center);
	glm_lookat(w->position, center, w->up, view);
	mat4 projection;
	glm_perspective(45.0f, (float)state.window_width/state.window_height, 0.001f, 1000.0f, projection);
	mat4 pv, mvp;
	glm_mat4_mul(projection, view, pv);
	mat4 model = GLM_MAT4_IDENTITY_INIT;
	glm_translate(model, &(vec3s){0.0f, CHUNK_SIZE_Y - 1, 0.0f});
	glm_mat4_mul(pv, model, mvp);
	glUniformMatrix4fv(uniform_model_mvp, 1, GL_FALSE, mvp);
  	glVertexAttribPointer(attribute_vertex, 3, GL_FLOAT, GL_FALSE, sizeof(f32)*8, 0);
  	glVertexAttribPointer(attribute_texcoord, 2, GL_FLOAT, GL_FALSE, sizeof(f32)*8, sizeof(f32)*3);
  	glVertexAttribPointer(attribute_normal, 3, GL_FLOAT, GL_FALSE, sizeof(f32)*8, sizeof(f32)*5);
  	glDrawArrays(GL_TRIANGLES, 0, *m->amounts);
}
