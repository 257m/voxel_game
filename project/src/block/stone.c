#include "block.h"

static u8 get_texture_location(enum Direction d)
{
    return TILE_STONE;
}

void stone_init()
{
    Block* stone = BLOCKS + STONE;
    stone->id = STONE;
    stone->transparent = false;
    stone->get_texture_location = get_texture_location;
}
