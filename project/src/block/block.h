#ifndef BLOCK_H
#define BLOCK_H

#include "../util.h"

enum BlockId {
    AIR,
    GRASS,
    DIRT,
    STONE,
    SAND,
    WATER,
    MAX_BLOCK,
};

enum TileId {
    TILE_GRASS_TOP,
    TILE_GRASS_SIDE,
    TILE_DIRT,
    TILE_STONE,
    TILE_SAND,
    TILE_WATER,
    TILE_MAX,
};

typedef struct {
	enum BlockId id;
	bool transparent;
	u8 (*get_texture_location)(enum Direction d);
} Block;

#define _BLOCK_DECL(_name)      \
    extern void _name##_init(); \
    _name##_init();

extern void block_init();

extern Block BLOCKS[MAX_BLOCK];

#endif /* BLOCK_H */
