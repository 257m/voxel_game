#include "block.h"

inline void block_init()
{
    _BLOCK_DECL(air);
    _BLOCK_DECL(grass);
    _BLOCK_DECL(dirt);
    _BLOCK_DECL(stone);
    _BLOCK_DECL(sand);
    _BLOCK_DECL(water);
}

Block BLOCKS[MAX_BLOCK];
