#include "block.h"

void air_init()
{
    Block* air = BLOCKS + AIR;
    air->id = AIR;
    air->transparent = true;
    // Air will not have a texture location
    air->get_texture_location = NULL;
}
