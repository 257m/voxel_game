#include "errors.h"
#include "world.h"

void chunk_queue_init(Chunk_Queue *cq, World* w, void (*poll_func)(World* w, Chunk* c), u64 event_size, u8 queue_mask)
{
	cq->chunks = malloc(sizeof(Chunk*)*event_size);
	PANIC(!cq->chunks, "Failed to allocate chunk queue!\n");
	cq->size = event_size;
	chunk_queue_flush(cq);
	cq->queue_mask = queue_mask;
	cq->w = w;
	cq->poll_func = poll_func;
}

void chunk_queue_push(Chunk_Queue* cq, Chunk* c)
{	
	if (c->is_queued)
		return;
	c->is_queued |= cq->queue_mask;
	cq->chunks[cq->tail++] = c;
	cq->tail %= cq->size;
}

void chunk_queue_poll(Chunk_Queue* cq)
{
	if (cq->head == cq->tail)
		return;
	Chunk* c = cq->chunks[cq->head];
	c->is_queued &= ~cq->queue_mask;
	if (c->scrapped && !c->is_queued) {
		HASH_DEL(cq->w->chunks, c);
		chunk_free(c);
	}
	else if (!c->is_queued)
		cq->poll_func(cq->w, c);
	cq->head = ++cq->head % cq->size;
}

void chunk_queue_flush(Chunk_Queue* cq)
{
	cq->head = 0;
	cq->tail = 0;
}
