#ifndef UTIL_H
#define UTIL_H

#include <math.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <limits.h>

#include "cglm/cglm.h"
#include "cglm/struct.h"

#include "data_types.h"

enum Direction {
    NORTH = 0,
    SOUTH = 1,
    EAST = 2,
    WEST = 3,
    UP = 4,
    DOWN = 5,
    POS_Z = 0,
    NEG_Z = 1,
    POS_X = 2,
    NEG_X = 3,
	POS_Y = 4,
	NEG_Y = 5,
};

#endif /* UTIL_H */
