#ifndef WORLD_H
#define WORLD_H

#include <GL/gl.h>
#include <stdbool.h>
#include "data_types.h"
#include "block/block.h"
#include "cglm/vec3.h"
#include "uthash.h"

#define CHUNK_SIZE_H 16
#define CHUNK_SIZE_V 32
#define CHUNK_SIZE_X CHUNK_SIZE_H
#define CHUNK_SIZE_Y CHUNK_SIZE_V
#define CHUNK_SIZE_Z CHUNK_SIZE_H
#define CHUNK_SIZE (CHUNK_SIZE_X * CHUNK_SIZE_Y * CHUNK_SIZE_Z)

typedef struct Chunk_t {
	u16 blk [CHUNK_SIZE];	
	GLuint vbo;
	u32 elements;
	ivec2s id;
	UT_hash_handle hh;
	u8 is_queued;
	u8 generated : 1;
	u8 changed : 1;
	u8 need_to_save : 1;
	u8 scrapped : 1;
} Chunk;

struct World_t;
typedef struct World_t World;

#include "chunk_sort.h"
#include "chunk_queue.h"

typedef struct {
	u32 height_map;
	u32 heat_map;
	u32 humidity_map;
	u32 structure_map;
} World_Seed;

typedef struct World_t {
	Chunk_Queue cuq;
	Chunk_Queue cgq;
    World_Seed seed;
	Chunk* chunks;
	Sorted_Chunk* sorted_chunks;
	vec3 position;
	vec3 velocity;
	vec3 forward;
    vec3 right;
    vec3 up;
    vec3 lookat;
    vec3 angle;
	vec2 old_position;
	u8 rend_dist;
} World;

extern void update_vectors(f64 dx, f64 dy);
extern void chunk_generate(World* w, Chunk* c);
extern void chunk_free(Chunk* c);
extern void chunk_update(World* w, Chunk* c);
extern void chunk_render(World* w, Chunk* c);
extern inline void chunk_set(Chunk* c, u8 x, u8 y, u8 z, u16 s);
extern inline void chunk_set_raw(Chunk* c, u32 blk, u16 s);
#define chunk_get(c, x, y, z) c->blk[y*CHUNK_SIZE_X*CHUNK_SIZE_Z + x*CHUNK_SIZE_Z + z]
#define chunk_get_block(c, x, y, z) (c->blk[y*CHUNK_SIZE_X*CHUNK_SIZE_Z + x*CHUNK_SIZE_Z + z] & 0xFF)

extern void world_init(World* w, u8 rend_dist);
extern void world_logic(World* w);
extern void world_render(World* w);
extern void world_seed_generate(World_Seed* w);

#endif /* WORLD_H */
