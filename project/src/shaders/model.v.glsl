#version 120

attribute vec3 vertex;
attribute vec2 texcoord;
attribute vec3 normal;
varying vec2 vtexcoord;
varying vec3 vnormal;
uniform mat4 mvp;

void main(void) {
        gl_Position = mvp * vec4(vertex.xyz, 1);
        vtexcoord = texcoord;
        vnormal = normal;
}
