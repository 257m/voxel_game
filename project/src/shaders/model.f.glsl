#version 120
varying vec2 vtexcoord;
varying vec3 vnormal;
uniform sampler2D texture;

void main(void)
{
        float difference = 3 - vnormal.x - vnormal.y - vnormal.z;
        gl_FragColor = texture2D(texture, vtexcoord)*difference;
}
