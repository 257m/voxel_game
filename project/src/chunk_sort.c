#include "world.h"
#include "util.h"

static inline void swap(Sorted_Chunk* a, Sorted_Chunk* b)
{
	Sorted_Chunk t = *a;
	*a = *b;
	*b = t;
}

static inline int partition(Sorted_Chunk* arr, int low, int high)
{
	// Smaller element index
	int i = low;

	// Loop through partition and swap smaller element with current
	for (int j = low; j < high; j++)
		if (arr[j].dist < arr[high].dist)
			swap(arr + i++, arr + j);

	// Swap the smallest element and the pivot and return the partition divider index
	swap(arr + i, arr + high);
	return i;
}

void quicksort(Sorted_Chunk* arr, int low, int high)
{
	// If the partition size is zero return
	if (low >= high)
		return;

	// Item in middle of the two partitions
	// mid is already in the right place
	int mid = partition(arr, low, high);
	
	// Sort both parititon/halfs
	quicksort(arr, low, mid - 1);
	quicksort(arr, mid + 1, high);
}
