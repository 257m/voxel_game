#ifndef DATA_TYPES_H
#define DATA_TYPES_H

#include <stdint.h>

typedef int8_t i8;
typedef int16_t i16;
typedef int32_t i32;
typedef int64_t i64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef float f32;
typedef double f64;

// Some missing types from cglm
typedef unsigned int uvec2 [2];
typedef u8 u8vec2 [2];

typedef u8 u8vec3 [3];
typedef union {
	u8vec3 raw;
	struct {
		u8 x, y, z;
	};
} u8vec3s;

typedef u8 u8vec4 [4];
typedef union {
	u8vec4 raw;
	struct {
		u8 x, y, z, w;
	};
} u8vec4s;

#define member(type, member) ((type *)0)->member
#define member_size(type, member) sizeof(((type *)0)->member)

#endif /* DATA_TYPES_H */
